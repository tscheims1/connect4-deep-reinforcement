import numpy as np
from Env import Env

class ExpMemory:
	
	def __init__(self,model,maxSize):
		self.model = model
		self.maxSize = maxSize
		self.memory = []
		self.discount = 0.9
		
	def add(self,state,action,reward,nextState):
		
		self.memory.append([state,action,reward,nextState])
		if len(self.memory) > self.maxSize:
			del self.memory[0]
		
	def getBatch(self,batchSize):
		
		batch = []
		
		inputs = np.zeros((min(len(self.memory),batchSize),7,6,1))
		targets = np.zeros((inputs.shape[0],7))
		
		batchNo = np.random.randint(0, len(self.memory),size=inputs.shape[0])
		
		for i,idx in enumerate(batchNo):
			
			state,action,reward,nextState = self.memory[idx]
			
			targets[i] = self.model.predict(state[np.newaxis,...,np.newaxis])
			
			
			inputs[i] = state[...,np.newaxis]
			if(nextState is not None):
				qSa = self.model.predict(nextState[np.newaxis,...,np.newaxis]).max()
				targets[i,action] = reward + self.discount *  qSa
			else:
				targets[i, action] = reward
		
		return inputs,targets
			
			
