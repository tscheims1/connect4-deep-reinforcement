from Bot import Bot
from Env import Env
from ExpMemory import ExpMemory
import numpy as np

from keras.models import Sequential
from keras.layers.core import Dense
from keras.optimizers import sgd
from keras.layers import Conv2D
from keras.layers.core import Dense, Flatten
from RandomBot import RandomBot
import re

class RLBot(Bot):
	
	def __init__(self,playerNo):
		super(RLBot, self).__init__(playerNo)
		
		self.bot1 = RandomBot(1)
		hiddenSize = 100
		
	
		self.model = Sequential()
		self.model.add(Conv2D(32, (4,4), input_shape=(7,6,1), activation='relu'))
		#self.model.add(Conv2D(16, (3,3), activation='relu'))
		self.model.add(Flatten())
		self.model.add(Dense(100,activation='relu'))
		self.model.add(Dense(100,activation='relu'))
		self.model.add(Dense(7))
		self.model.compile(sgd(lr=.02), "mse")
		self.model.summary()
		
		self.epsilon = 0.1
		self.loss = 0.0
		self.maxMemory = 10000
		self.expMemory = ExpMemory(self.model,self.maxMemory)
		self.counter = 0
		self.batchSize = 50
		
		self.turns = 0
		self.gameNo =0
	
	def doAction(self,env):
		
		self.turns += 1
		
		q = self.model.predict(env.getState()[np.newaxis,...,np.newaxis])
		
		if np.random.rand() <= self.epsilon:
			action =  np.random.randint(7)	    
		else:
			action = np.argmax(q[0])
		
		
		if(not env.isValidAction(action) or env.getReward(env.getState(),self.playerNo) == -1):
			newState = None
			reward = -1
		else:
			
			newState = env.getNewState(env.getState().copy(),action,self.playerNo)
			reward = env.getReward(newState,self.playerNo)
			"""if(reward == 0):
				tmpEnv = Env()
				tmpEnv.board = newState
				
				tmpEnv.applyAction(self.bot1.doAction(tmpEnv),self.bot1.playerNo)
				reward = tmpEnv.getReward(tmpEnv.getState(),self.playerNo)
				newState = tmpEnv.getState()
			"""
				
		
		self.expMemory.add(env.getState(),action,reward,newState)
		
		if(len(self.expMemory.memory) >= self.batchSize):
			inputs, targets = self.expMemory.getBatch(self.batchSize)
		
			self.loss += self.model.train_on_batch(inputs, targets)
			self.counter+=1
		
		return action
	
	def restart(self):
		#if(self.turns != 0):
			#print("loss:"+str(self.loss/float(self.turns)))
		self.loss=0
		self.turns = 0
		self.gameNo+=1
		
		if(self.gameNo %10000 ==0):
			modelName = "model"+str(self.gameNo)+".h5"
			print("Saving the model to "+modelName)
			jsonString = self.model.to_json()
			open('model.json', 'w').write(jsonString)
			self.model.save_weights(modelName)
			
	def loadModel(self,weights):
		if(weights !=''):
			self.model.load_weights(weights)
			m = re.search('\d+',weights.split('.')[0])
			if(m is not None):
				self.gameNo = int(m.group(0))
			
