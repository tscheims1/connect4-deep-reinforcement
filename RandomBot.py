from Bot import Bot
from Env import Env
import numpy as np

class RandomBot(Bot):
	
	def __init__(self,playerNo):
		super(RandomBot, self).__init__(playerNo)
		
	
	def doAction(self,env):
		actions = env.getAllAvailableActions()
		np.random.shuffle(actions)
		return actions[0]
	
	def restart(self):
		r = 0
