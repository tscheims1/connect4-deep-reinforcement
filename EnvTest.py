import unittest
from Env import Env
import numpy as np

class EnvTest(unittest.TestCase):
	
	def testRewardHorizontal(self):
		env = Env()
		
		board = np.zeros((7,6),dtype=np.int)
		board[0:4,0:1] = np.transpose(np.array([[1,1,1,1]]))
		
		self.assertEqual(env.getReward(board,1),1)
		self.assertEqual(env.getReward(board,2),-1)
		
		
		board = np.zeros((7,6),dtype=np.int)
		board[3:7,5:6] = np.transpose(np.array([[1,1,1,1]]))
		
		self.assertEqual(env.getReward(board,1),1)
		self.assertEqual(env.getReward(board,2),-1)
		
		board = np.zeros((7,6),dtype=np.int)
		board[3:7,5:6] = np.transpose(np.array([[2,2,2,2]]))
		
		self.assertEqual(env.getReward(board,1),-1)
		self.assertEqual(env.getReward(board,2),1)
		
	
	def testRewardVertical(self):
		env = Env()
		
		board = np.zeros((7,6),dtype=np.int)
		board[0:1,0:4] = np.array([[1,1,1,1]])
		
		self.assertEqual(env.getReward(board,1),1)
		self.assertEqual(env.getReward(board,2),-1)
		
		
		board = np.zeros((7,6),dtype=np.int)
		board[6:7,2:6] = (np.array([[1,1,1,1]]))
		
		self.assertEqual(env.getReward(board,1),1)
		self.assertEqual(env.getReward(board,2),-1)
		
		board = np.zeros((7,6),dtype=np.int)
		board[6:7,2:6]= (np.array([[2,2,2,2]]))
		
		self.assertEqual(env.getReward(board,1),-1)
		self.assertEqual(env.getReward(board,2),1)
		
	def testRewardDiagnoal(self):
		env = Env()
		
		board = np.array([
			[1, 0, 0, 0, 0, 0],
			[0 ,1 ,2 ,2 ,0 ,0],
			[2 ,2 ,1 ,2 ,0 ,0],
			[2 ,2 ,2 ,1 ,0 ,0],
			[0 ,0, 0 ,0 ,0 ,0],
			[0 ,0 ,0 ,0 ,0 ,0],
			[0 ,0 ,0 ,0 ,0 ,0]],dtype=np.int)
		
		self.assertEqual(env.getReward(board,1),1)
		self.assertEqual(env.getReward(board,2),-1)
		
		board = np.array([
			[0, 0, 0, 0, 0, 0],
			[0 ,1 ,2 ,2 ,0 ,0],
			[2 ,2 ,1 ,2 ,0 ,0],
			[2 ,2 ,2 ,1 ,0 ,0],
			[0 ,0, 0 ,0 ,1 ,0],
			[0 ,0 ,0 ,0 ,0 ,0],
			[0 ,0 ,0 ,0 ,0 ,0]],dtype=np.int)
		
		self.assertEqual(env.getReward(board,1),1)
		self.assertEqual(env.getReward(board,2),-1)
		
		board = np.array([
			[0, 0, 0, 0, 0, 0],
			[0 ,0 ,2 ,2 ,0 ,0],
			[2 ,2 ,1 ,2 ,0 ,0],
			[2 ,2 ,2 ,1 ,0 ,0],
			[0 ,0, 0 ,0 ,1 ,0],
			[0 ,0 ,0 ,0 ,0 ,1],
			[0 ,0 ,0 ,0 ,0 ,0]],dtype=np.int)
		
		self.assertEqual(env.getReward(board,1),1)
		self.assertEqual(env.getReward(board,2),-1)
		
		board = np.array([
			[0, 0, 0, 0, 0, 0],
			[0 ,0 ,2 ,2 ,0 ,0],
			[2 ,2 ,1 ,2 ,0 ,0],
			[1 ,2 ,2 ,1 ,0 ,0],
			[0 ,1, 0 ,0 ,0 ,0],
			[0 ,0 ,1 ,0 ,0 ,1],
			[0 ,0 ,0 ,1 ,0 ,0]],dtype=np.int)
		
		self.assertEqual(env.getReward(board,1),1)
		self.assertEqual(env.getReward(board,2),-1)
		
		board = np.array([
			[0, 0, 0, 0, 0, 0],
			[0 ,0 ,2 ,2 ,0 ,0],
			[1 ,2 ,1 ,2 ,0 ,0],
			[1 ,1 ,2 ,1 ,0 ,0],
			[0 ,1, 1 ,0 ,0 ,0],
			[0 ,0 ,0 ,1 ,0 ,1],
			[0 ,0 ,0 ,1 ,0 ,0]],dtype=np.int)
			
		self.assertEqual(env.getReward(board,1),1)
		self.assertEqual(env.getReward(board,2),-1)
		
		board = np.array([
			[0, 0, 0, 0, 0, 0],
			[0 ,0 ,2 ,2, 1 ,0],
			[1 ,2 ,1 ,1 ,0 ,0],
			[1 ,0 ,1 ,1 ,0 ,0],
			[0 ,1, 0 ,0 ,0 ,0],
			[0 ,0 ,0 ,1 ,0 ,1],
			[0 ,0 ,0 ,1 ,0 ,0]],dtype=np.int)
		
					
		self.assertEqual(env.getReward(board,1),1)
		self.assertEqual(env.getReward(board,2),-1)
		
		board = np.array([
			[0, 0, 0, 0, 0, 0],
			[0 ,0 ,2 ,2, 1 ,0],
			[1 ,2 ,1 ,1 ,0 ,0],
			[1 ,0 ,0 ,1 ,0 ,0],
			[0 ,1, 0 ,0 ,0 ,0],
			[0 ,0 ,0 ,1 ,0 ,1],
			[0 ,0 ,0 ,1 ,0 ,0]],dtype=np.int)
		
		self.assertEqual(env.getReward(board,1),0)
		self.assertEqual(env.getReward(board,2),0)
		
		
		print(board)
		print(np.diag(np.fliplr(board),0))
		
if __name__ == '__main__':
    unittest.main()
