from Env import Env
from RandomBot import RandomBot
from RLBot import RLBot
import sys
import numpy as np
import argparse
import re

ap = argparse.ArgumentParser()
ap.add_argument('--weights',type=str,default='',required=False)
args = vars(ap.parse_args())                      
	


sys.stdout = open('log.txt', 'w')

bot1 = RandomBot(1)
bot2 = RLBot(2)
bot2.loadModel(args['weights'])
isRunning = True

bot1Wins = 0
bot2Wins = 0
draw = 0
gameCount = 100
epoche = 1000000

bot1Wins = 0
bot2Wins = 0
draw = 0
invalidTurns =0

currentInvalidTurns = 0
currentBot1Wins = 0
currentBot2Wins = 0

actionCount = np.zeros(7)
turnNo = 0

epocheStart = 0
m = re.search('\d+',args['weights'].split('.')[0])
if(m is not None):
	epocheStart = int(m.group(0))
	
for i in range(epocheStart,epoche):
	
	env = Env()
	bot1.restart()
	bot2.restart()
	if(i%100 == 0):
		currentBot1Wins = 0
		currentBot2Wins = 0
		currentInvalidTurns = 0
		turnNo = 0
		actionCount = np.zeros(7)
	
	while True:
		
		action = bot1.doAction(env)
		if(not env.isValidAction(action)):
			bot2Wins +=1
			currentBot2Wins +=1
			break
		
		env.applyAction(action,bot1.playerNo)
		if(env.getReward(env.getState(),bot1.playerNo == 1)):
			bot1Wins+=1
			currentBot1Wins +=1
			bot2.doAction(env) # just add to training set
			break
		
		action =bot2.doAction(env)
		actionCount[action]+=1
		if(not env.isValidAction(action)):
			bot1Wins +=1
			currentBot1Wins +=1 
			invalidTurns+=1
			currentInvalidTurns+=1
			break
		
		
		env.applyAction(action,bot2.playerNo)
		if(env.getReward(env.getState(),bot2.playerNo == 1)):
			bot2Wins+=1
			currentBot2Wins +=1
			break
		
		if(env.isFinished()):
			draw += 1
			break
	
	turnNo += env.turnNo
		
	if(i%100 == 99):
		print("epoche "+str(i+1)+"\t"+str(bot1Wins)+ "\t" + str(draw)+"\t"+str(bot2Wins)+"\t"+str(invalidTurns)+"\t"+str(round(float(currentBot2Wins)/(currentBot2Wins+currentBot1Wins),2))+"\t"+str(currentInvalidTurns)+"\t"+str(turnNo/100)+"\t"+str(actionCount))
		
		sys.stdout.flush()
		
		
		
