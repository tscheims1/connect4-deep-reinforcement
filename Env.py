import numpy as np

class Env:

	def __init__(self):
		self.board = np.zeros((7,6),dtype=int)
		self.turnNo =0
		
	def __str__(self):
		return str(np.transpose(self.board))
	
	def isValidAction(self,action):
		return  action in self.getAllAvailableActions()
	
	def getAllAvailableActions(self):
		
		actions = []
		for i in range(7):
			if(self.board[i][5] ==0):
				actions.append(i)
				
		return actions
				
	
	def getState(self):
		return self.board
	
	def getNewState(self,state,action,player):
		if(self._applyAction(state,action,player)):
			return state
		else:
			return None
		
	def _applyAction(self,state,action,player):
		for i in range(6):
			if(state[action][i] ==0):
				state[action][i] = player
				return True
			
		return False
	def applyAction(self,action,player):
		if(self._applyAction(self.board,action,player)):
			self.turnNo += 1
		
			
	def isFinished(self):
		return self.turnNo == 42
	
	def getReward(self,state,player):
		
		if(player == 1):
			checkWin = 1
			checkLoose = 2
		else:
			checkWin = 2
			checkLoose = 1
			
		checkWinArr = np.array([[checkWin,checkWin,checkWin,checkWin]])
		checkLoseArr = np.array([[checkLoose,checkLoose,checkLoose,checkLoose]])
		
		#horizontal
		for j in range(6):
			for i in range(7-3):

				if(np.array_equal(state[i:i+4,j:j+1],np.transpose(checkWinArr))):
					return 1
				if(np.array_equal(state[i:i+4,j:j+1],np.transpose(checkLoseArr))):
					return -1
		
		#vertical
		for j in range(6-3):
			for i in range(7):

				if(np.array_equal(state[i:i+1,j:j+4],checkWinArr)):
					return 1
				if(np.array_equal(state[i:i+1,j:j+4],checkLoseArr)):
					return -1
				
		#diag 1
		for j in range(-3,2):
			diag = np.diag(state,j)
			for i in range(len(diag)-3):
				
				if(np.array_equal(diag[i:i+4],checkWinArr[0])):
					return 1
				if(np.array_equal(diag[i:i+4],checkLoseArr[0])):
					return -1
			
		flippedState = np.fliplr(state)
		for j in range(-3,2):
			diag = np.diag(flippedState,j)
			for i in range(len(diag)-3):
				if(np.array_equal(diag[i:i+4],checkWinArr[0])):
					return 1
				if(np.array_equal(diag[i:i+4],checkLoseArr[0])):
					return -1
				
		return 0
