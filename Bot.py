class Bot(object):
	
	def __init__(self,playerNo):
		self.playerNo = playerNo
		
	
	def doAction(self,env):
		raise NotImplementedError
	
	def restart(self):
		raise NotImplementedError
