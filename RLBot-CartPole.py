import random
import gym
import numpy as np
from collections import deque
from keras.models import Sequential
from keras.layers import Dense
from keras.optimizers import Adam
from keras.optimizers import sgd
import math


EPISODES = 5000


class RLBot:
	
	def __init__(self,stateSize,actionSize):
		self.model = self.createModel(stateSize,actionSize)
		self.targetModel = self.createModel(stateSize,actionSize)
		
		self.epsilon = 1.0
		self.epsilonDecay =0.995
		self.epsilonMin = 0.01
		self.loss = 0.0
		self.maxMemory = 2000
		self.targetNetworkUpdate = 10000
		self.currentSampleNo = 0
		self.memory = ExpMemory(self.model,self.targetModel,self.maxMemory)
		self.actionSize = actionSize
		
		
	def createModel(self,stateSize,actionSize):
		model = Sequential()
		#self.model.add(Conv2D(32, (4,4), input_shape=(7,6,1), activation='relu'))
		#self.model.add(Conv2D(16, (3,3), activation='relu'))
		#self.model.add(Flatten())
		
		model.add(Dense(100,input_dim=stateSize,activation='relu'))
		model.add(Dense(100,activation='relu'))
		model.add(Dense(actionSize, activation='linear'))
		model.compile(loss='mse',
                      optimizer=Adam(lr=0.001))
		model.summary() 
		return model
	
	def updateTargetModel(self):
		
		self.targetModel.set_weights(self.model.get_weights())
		
	def act(self,state):
		if np.random.rand() <= self.epsilon:
			return random.randrange(self.actionSize)
		act_values = self.model.predict(state)
		return np.argmax(act_values[0])  # returns action
	
	def remember(self,state,action,reward,nextState):
		self.memory.remember(state,action,reward,nextState)
		
	def replay(self,batchSize):
		if(len(self.memory.memory) == self.maxMemory):
			self.memory.replay(batchSize)
			self.currentSampleNo += batchSize
			if(self.currentSampleNo >= self.targetNetworkUpdate):
				self.currentSampleNo = 0
				self.updateTargetModel()
			return True
		return False
	
	def memorySize(self):
		return self.memory.size()
	def decayExploration(self):
		if self.epsilon > self.epsilonMin:
			self.epsilon *= self.epsilonDecay
	
class ExpMemory:
	
	def __init__(self,model,targetModel,maxSize):
		self.model = model
		self.targetModel = targetModel
		self.maxSize = maxSize
		self.memory = []
		self.discount = 0.995
		self.alpha = 0
		self.epsilon = 0.01
		self.currentPos = 0
		
		self.denseLoss = np.zeros(self.maxSize)
		#self.denseLoss = np.array([(i+self.epsilon)**self.alpha for i in self.denseLoss])
	
	def size(self):
		return len(self.memory)
		
	def remember(self,state,action,reward,nextState):
		
		
		
		if len(self.memory) == self.maxSize:
			self.memory[self.currentPos] = [state,action,reward,nextState]
			self.denseLoss[self.currentPos] = (abs(reward) +self.epsilon) ** self.alpha
			self.currentPos+=1
			self.currentPos %= self.maxSize
		else:
			self.memory.append([state,action,reward,nextState])
			self.denseLoss[len(self.memory)-1] = (abs(reward) +self.epsilon) ** self.alpha
		
	def replay(self,batchSize):
		
		#self.denseLoss = np.ones(self.maxSize)
		
		minibatchSerie = np.random.choice(range(0,len(self.memory)),batchSize,p=self.denseLoss/np.sum(self.denseLoss))
		
		
		targets = []
		states = []
		
		
		
		
		for no in minibatchSerie:
			
			state,action,reward,nextState = self.memory[no]
			
			target = self.model.predict(state)
			
			if(nextState is None):
				target[0][action] = reward
			else:
				a = self.model.predict(nextState)[0]
				t = self.targetModel.predict(nextState)[0]
				target[0][action] = reward + self.discount * t[np.argmax(a)]
			
			targets.append(target[0])
			states.append(state[0])
		
			hist = self.model.fit(state, target, epochs=1, verbose=0)
			self.denseLoss[no] = (hist.history['loss'][0] + self.epsilon)**self.alpha
		
		
		
		#self.model.train_on_batch(np.array(states), np.array(targets))

if __name__ == "__main__":
	env = gym.make('CartPole-v1')
	state_size = env.observation_space.shape[0]
	action_size = env.action_space.n
	agent = RLBot(state_size, action_size)
	# agent.load("./save/cartpole-dqn.h5")
	done = False
	batch_size = 32
	
	for e in range(EPISODES):
		state = env.reset()
		state = np.reshape(state, [1, state_size])
		for time in range(500):
			# env.render()
			
			action = agent.act(state)
			next_state, reward, done, _ = env.step(action)
			reward = reward if not done else -10
			next_state = np.reshape(next_state, [1, state_size]) if not done else None
			
			agent.remember(state, action, reward, next_state)
			state = next_state
			if done:
				#agent.updateTargetModel()
				print("episode: {}/{}, score: {}, e: {:.2}"
						.format(e, EPISODES, time, agent.epsilon))
				break
		#if agent.memorySize() > batch_size:
		if(agent.replay(batch_size)):
			agent.decayExploration()
